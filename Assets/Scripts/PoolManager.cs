using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static PoolManager m_Instance;
    public static PoolManager Instance => m_Instance;

    [SerializeField]
    private Pool _Enemy1Pool;
    //[SerializeField] 
    //private Pool _Enemy2Pool;
    public Pool Enemy1Pool { get { return _Enemy1Pool; } }
    //public Pool Enemy2Pool { get { return _Enemy2Pool; } }
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }
}
