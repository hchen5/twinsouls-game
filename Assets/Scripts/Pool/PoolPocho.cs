using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PoolPocho : MonoBehaviour
{
    private List<GameObject> m_Pool;    
    public bool ReturnElement(GameObject element)
    {
        if(m_Pool.Contains(element))
        {
            element.SetActive(false);
            return true;
        }
        return false;
    }

    public GameObject GetElement()
    {
        foreach (GameObject objeto in m_Pool)
            if (!objeto.activeInHierarchy)
            {
                objeto.SetActive(true);
                return objeto;
            }
        return null;
    }

    private void Awake()
    {
        m_Pool = new List<GameObject>();
        int children = transform.childCount;
        for (int i = 0; i < children; ++i)
            m_Pool.Add(transform.GetChild(i).gameObject);
    }
}
