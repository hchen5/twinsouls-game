using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EconomyManager
{
    public class Economy
    {
        private float _Value;
        private float _MaxValue;
        private float _MinValue;
        public float Value { get {return _Value; } }
        public float MaxValue { get {return _MaxValue; } }
        public float MinValue { get {return _MinValue; } }
        private Dictionary<string, Coroutine> _Coroutines;
        public delegate void Warn();
        public Warn WarnWhenMinValue;
        public Warn WarnWhenMaxValue;
        public Warn WarnEconomyUpdated;
        public Economy(float InitialValue, float MinValue, float MaxValue)
        {
            _Value = InitialValue;
            _MaxValue = MaxValue;
            _MinValue = MinValue;
            _Coroutines = new Dictionary<string, Coroutine>();
        }
        public void add(float Number)
        {
            if (_Value + Number > _MaxValue)
            {
                _Value = _MaxValue;
                WarnWhenMaxValue?.Invoke();
            }
            else if (_Value + Number < _MinValue)
            {
                _Value = _MinValue;
                WarnWhenMinValue?.Invoke();
            }
            else
                _Value += Number;
            WarnEconomyUpdated?.Invoke();
        }
        public void IncreaseMaxvalue(float Number)
        {
            _MaxValue += Number;
        }
        public void DecreaseMinvalue(float Number)
        {
            _MinValue -= Number;
        }
        public void addPerSeconds(float Number, string Identifier, MonoBehaviour instance)
        {
            if (!_Coroutines.ContainsKey(Identifier))
                _Coroutines.Add(Identifier, instance.StartCoroutine(addPerSecondsCoroutine(Number)));
        }
        public void StopAddPerSeconds(string Identifier, MonoBehaviour instance)
        {
            if (_Coroutines.ContainsKey(Identifier))
            {
                instance.StopCoroutine(_Coroutines[Identifier]);
                _Coroutines.Remove(Identifier);
            }
        }
        public void addPerSecondsSince(float Number, float Since, string Identifier, MonoBehaviour instance)
        {
           instance.StartCoroutine(addPerSecondsSinceCoroutine(Number, Since, Identifier, instance));
        }
        private IEnumerator addPerSecondsSinceCoroutine(float Number, float Since, string Identifier, MonoBehaviour instance)
        {
            addPerSeconds(Number, Identifier, instance);
            yield return new WaitForSeconds(Since);
            StopAddPerSeconds(Identifier, instance);
        }
        private IEnumerator addPerSecondsCoroutine(float Number)
        {
            while (true)
            {
                add(Number/10);
                yield return new WaitForSeconds(.1f);
            }
        }
    }
}

