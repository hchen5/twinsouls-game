using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemsScriptableObjects", menuName = "ScriptableObjects/ItemsScriptableObjects")]
public class ItemScriptableObject : ScriptableObject
{
    public int id;
    public enum ItemType {Usable, Object}
    public ItemType _MyTypyItem;
    public float _GainHealth;
    public float _GainStamina;
    public float _GainMana;
    public Sprite ItemImage;
}
