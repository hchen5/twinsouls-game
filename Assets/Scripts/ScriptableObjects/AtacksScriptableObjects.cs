using System.Collections;
using System.Collections.Generic;
using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;
using static SpecialAtacks.Atacks;

[CreateAssetMenu(fileName = "AtacksScriptableObjects", menuName = "ScriptableObjects/AtacksScriptableObjects")]
public class AtacksScriptableObjects : ScriptableObject
{
    public AllAtacksName Atack;
    public float CoolDownTime;
    [Header("Sprites")]
    public Sprite AtackImage;
    [Header("Economy")]
    public float DrainedStamina;
    public float DrainedMana;
    public float DrainedHealth;
    public float velocity;
    public float Damage;
}
