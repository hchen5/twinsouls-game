using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PruebaMovimientoPlayer : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset _PlayerActionAssets;
    private InputActionAsset _PlayerInputs;
    private InputAction _MovementAction;
    public InputAction MovementAction => _MovementAction;
    private Rigidbody2D _Rigidbody2D;
    private Vector2 _Movement;
    private void Awake()
    {
        _PlayerInputs = Instantiate(_PlayerActionAssets);
        _PlayerInputs.FindActionMap("PlayerOne").Enable();
        _MovementAction = _PlayerInputs.FindActionMap("PlayerOne").FindAction("Move");
        _Rigidbody2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        _Movement = _MovementAction.ReadValue<Vector2>();
        _Rigidbody2D.velocity = _Movement.normalized * 5f;
    }
}
