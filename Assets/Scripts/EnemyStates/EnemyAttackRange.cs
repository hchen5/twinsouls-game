using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackRange : MonoBehaviour
{
    private bool _PlayerInRange = false;
    public bool PlayerInRange { get { return _PlayerInRange; } }
    private Transform _Target;
    public Transform Target { get { return _Target; } }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _PlayerInRange = true;
            _Target = collision.transform;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            _PlayerInRange = false;
            _Target = null;
        }
    }

}
