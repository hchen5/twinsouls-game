using Animations;
using enemy;
using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace enemy
{
    public class SMEnemyFollowState : MBState
    {
        private SMEnemyBehaviour _Enemy;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        private SpriteRenderer _SpriteRenderer;
        private bool _PlayerInRange = false;
        private bool _PlayerInRangeAttack = false;
        private Transform _PlayerTransform;
        [SerializeField]
        private float _DistanceToStopFollow = 1.2f;
        [SerializeField]
        private float _Speed = 3f;
        //[SerializeField]
        //private float _FollowColliderRadius = 4f;
        private CircleCollider2D _FollowCircleCollider;
        private AnimatorManager _AnimatorManager;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Enemy = GetComponent<SMEnemyBehaviour>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _FollowCircleCollider = GetComponentInChildren<EnemyFollowRange>().transform.GetComponent<CircleCollider2D>();
            _Speed = _Enemy.Speed;
            _AnimatorManager = new AnimatorManager(GetComponent<Animator>());
        }
        private void Start()
        {
            _FollowCircleCollider.radius = _Enemy.ColliderFollow;
        }
        public override void Init()
        {
            base.Init();
            _AnimatorManager.SetAnimation(GetComponent<SMEnemyAttackState>().AtackDirection(), AnimatorManager.AimationType.Movement);
            _Speed = _Enemy.Speed;
            //_SpriteRenderer.color = Color.red; 
            _FollowCircleCollider.radius = _Enemy.ColliderFollow;
        }
        public override void Exit()
        {
            base.Exit();
        }
        private void FixedUpdate()
        {
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _PlayerTransform = GetComponentInChildren<EnemyFollowRange>().Target;
            _PlayerInRangeAttack = GetComponentInChildren<EnemyAttackRange>().PlayerInRange;
            if (_PlayerInRange && !_PlayerInRangeAttack)
            {
                if (_PlayerTransform != null)
                {
                    Vector2 direction = _PlayerTransform.position - transform.position;
                    //RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, Vector2.Distance(transform.position, direction),~IgnoreLayer);
                    //if (hit.collider != null)
                    //{
                    //if (hit.collider.CompareTag("Player"))
                    //{
                    _Rigidbody.velocity = direction.normalized * _Speed;
                    //Debug.DrawRay(transform.position, direction, Color.red, 1f);
                    if (Vector2.Distance(_PlayerTransform.position, transform.position) <= _DistanceToStopFollow)
                        _Rigidbody.velocity = Vector2.zero;
                    _AnimatorManager.SetAnimation(GetComponent<SMEnemyAttackState>().AtackDirection(), AnimatorManager.AimationType.Movement);
                    //}else
                    //_StateMachine.ChangeState<SMEnemyIdleState>();
                    //}
                }
            }
            else if (_PlayerInRangeAttack)
            {
                _StateMachine.ChangeState<SMEnemyAttackState>();
                _Rigidbody.velocity = Vector2.zero;
            }
            else if (!_PlayerInRange & !_PlayerInRangeAttack)
            {
                _Rigidbody.velocity = Vector2.zero;
                _StateMachine.ChangeState<SMEnemyIdleState>();
            }
        }
    }
}
