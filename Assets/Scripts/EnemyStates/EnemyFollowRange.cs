using enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace enemy
{
    public class EnemyFollowRange : MonoBehaviour
    {
        private bool _PlayerInRange = false;
        public bool PlayerInRange { get { return _PlayerInRange; } }
        private Transform _Target;
        public Transform Target { get { return _Target; } }
        [SerializeField]
        private LayerMask IgnoreLayer;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                Vector2 direction = collision.transform.position - transform.position;
                RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, Vector2.Distance(transform.position, direction), ~IgnoreLayer);
                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag("Player"))
                    {
                        _PlayerInRange = true;
                        _Target = collision.transform;
                    }
                    else
                    {
                        _PlayerInRange = false;
                        _Target = null;
                    }
                }
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                Vector2 direction = collision.transform.position - transform.position;
                RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, Vector2.Distance(transform.position, direction), ~IgnoreLayer);
                if (hit.collider != null)
                {
                    //Debug.Log(hit.collider.name);
                    if (hit.collider.CompareTag("Player"))
                    {
                        _PlayerInRange = true;
                        _Target = collision.transform;
                    }
                    else
                    {
                        _PlayerInRange = false;
                        _Target = null;
                    }
                }
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                _PlayerInRange = false;
                _Target = null;
            }
        }
    }
}