using EconomyManager;
using FSMState;
using SpecialAtacks;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.InputSystem;

namespace enemy
{
    [RequireComponent(typeof(NewFiniteStateMachine))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(SMEnemyIdleState))]
    [RequireComponent(typeof(SMEnemyFollowState))]
    [RequireComponent(typeof(SMEnemyPatrolingState))]
    [RequireComponent(typeof(SMEnemyAttackState))]
    
    public class SMEnemyBehaviour : MonoBehaviour
    {
        [SerializeField]
        private EnemyScriptableObject _EnemySO;
        private NewFiniteStateMachine _StateMachine;
        private int _Health;
        private float _Speed;
        private float _ColliderFollowRange;
        private float _ColliderAttackRange;
        private float _ColliderPatrolRange;
        private Economy Vida; 
        public float ColliderFollow { get => _ColliderFollowRange; }
        public float ColliderAttack { get => _ColliderAttackRange; }
        public float ColliderPatrol { get => _ColliderPatrolRange; }
        public float Speed { get => _Speed; }
        public bool Camp { get => _Camp; set => _Camp = value; }
        public bool Dead1 { get => _Dead; set => _Dead = value; }

        public delegate void CampDead();
        public CampDead OnEnemyCampDead;

        private bool _Camp = false;
        private bool _Dead = false;


        private void Awake()
        {
           _StateMachine = GetComponent<NewFiniteStateMachine>();
            if (_EnemySO != null)
                EnemySO(_EnemySO);
        }
        public void Init(EnemyScriptableObject SO, Transform SpawnPosition, bool camp) 
        {
            _EnemySO = SO;
            EnemySO(_EnemySO);
            transform.position = SpawnPosition.position;
            Vida.WarnWhenMinValue += Dead;
            _Camp = camp;
            _Dead = false;
        }
        private void EnemySO(EnemyScriptableObject SO) 
        {
            _Health = SO.Health;
            _Speed = SO.Velocity;
            _ColliderPatrolRange = SO.ColliderPatrolRange;
            _ColliderFollowRange = SO.ColliderFollowRange;
            _ColliderAttackRange = SO.ColliderAttackRange;
            GetComponent<Animator>().runtimeAnimatorController = _EnemySO.Animator;
            Vida = new Economy(_Health,0,_Health);
            //transform.GetComponentInChildren<EnemyHitBox>().DMG = _Damage;
            
        }
        private void Start()
        {
           _StateMachine.ChangeState<SMEnemyIdleState>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("PlayerHitBox"))
            {
                if (collision.gameObject.GetComponent<AtackObjectScript>() != null)
                    Vida.add(-collision.gameObject.GetComponent<AtackObjectScript>().Damage);
                else
                    Vida.add(-1);
            }
        }
        private void Dead()
        {
            Vida.WarnWhenMinValue -= Dead;
            transform.GetComponent<Pooleable>().ReturnToPool();
            _Dead = true;
            OnEnemyCampDead?.Invoke();
        }
    }
}