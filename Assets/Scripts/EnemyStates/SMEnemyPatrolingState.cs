using Animations;
using enemy;
using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace enemy
{
    public class SMEnemyPatrolingState : MBState
    {
        private SMEnemyBehaviour _Enemy;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        private SpriteRenderer _SpriteRenderer;
        private bool _PlayerInRange = false;
        private bool _PlayerInRangeAttack = false;
        [SerializeField]
        private CircleCollider2D _OwnColliderPatrolCollider;
        [SerializeField]
        private float _DistanceToStopFollow = 0.5f;
        [SerializeField]
        private float _Speed = 3f;
        //private Coroutine _Coroutine = null;
        [SerializeField]
        private GameObject _PruebaPuntoGameObject;
        Vector3 point;
        [SerializeField]
        private CircleCollider2D _PatrolingCollider;
        //[SerializeField]
        //private float _PatrolingColliderRadius = 8f;
        private AnimatorManager _AnimatorManager;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Enemy = GetComponent<SMEnemyBehaviour>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _PatrolingCollider.radius = _Enemy.ColliderPatrol;
            _AnimatorManager = new AnimatorManager(GetComponent<Animator>());
        }
        public void SetCampamentCollider(CircleCollider2D collider) 
        {
            _PatrolingCollider = collider;
            _PatrolingCollider.radius = collider.radius;
        }
        public void OwnCollider(CircleCollider2D collider) 
        {
            _PatrolingCollider = collider;
            _PatrolingCollider.radius = _Enemy.ColliderPatrol;
        }
        public override void Init()
        {
            base.Init();
            //_Animator.Play("");
            //_SpriteRenderer.color = Color.green;
            if (_Enemy.Camp == false)
                OwnCollider(_OwnColliderPatrolCollider);
            point = RandomPointOnCircleEdge(_PatrolingCollider);
            //_PatrolingCollider.radius = _Enemy.ColliderPatrol;
            //ProbaPoints();
        }
        public override void Exit()
        {
            base.Exit();
        }
        private void ProbaPoints() 
        {
            GameObject g = Instantiate(_PruebaPuntoGameObject);
            g.transform.position = point;
            Destroy(g,5f);
        }
        private void FixedUpdate()
        {
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _PlayerInRangeAttack = GetComponentInChildren<EnemyAttackRange>().PlayerInRange;
            if (_PlayerInRange && !_PlayerInRangeAttack)
                _StateMachine.ChangeState<SMEnemyFollowState>();
            else if (_PlayerInRangeAttack)
                _StateMachine.ChangeState<SMEnemyAttackState>();
            else
            {
                if (Vector2.Distance(point, transform.position) <= _DistanceToStopFollow)
                {
                    _Rigidbody.velocity = Vector3.zero;
                    point = RandomPointOnCircleEdge(_PatrolingCollider);
                    //ProbaPoints();
                }
                else
                {
                    Vector2 direction = (point - transform.position).normalized;
                    _Rigidbody.velocity =  direction * _Speed;
                    _AnimatorManager.SetAnimation(PatrolDirection(), AnimatorManager.AimationType.Movement);
                }
            }
        }
        [SerializeField] private LayerMask IgnoreMask;
        private Vector2 RandomPointOnCircleEdge(CircleCollider2D collider)
        {

            Vector2 p = Vector2.zero;//= Point(collider);

            RaycastHit2D Ray2D;

            bool po = true;

            while (po)
            {
                p = Point(collider);
                Ray2D = Physics2D.Raycast(transform.position, (p - (Vector2)transform.position), Vector2.Distance(p, transform.position),
                ~IgnoreMask);
                Debug.DrawRay(transform.position, (p - (Vector2)transform.position), Color.red, 1f);
                if (Ray2D.collider != null)
                {
                    //Debug.Log(Ray2D.collider.gameObject.layer == LayerMask.NameToLayer("Obstacles"));
                    if (Ray2D.collider.gameObject.layer == LayerMask.NameToLayer("Obstacles"))
                        po = true;
                }
                else
                    po = false;
                    
            }
            return p;
        }
        private Vector2 Point(CircleCollider2D collider) 
        {
            // Obtenemos el centro del c�rculo y su radio desde el Collider
            Vector2 circleCenter = collider.transform.position;
            float circleRadius = collider.radius;

            // Generamos un �ngulo aleatorio en radianes
            float randomAngle = Random.Range(0f, 2f * Mathf.PI);

            // Generamos un radio aleatorio entre 0 y el radio del c�rculo
            float randomRadius = Random.Range(0f, circleRadius);

            // Calculamos las coordenadas cartesianas del punto dentro del c�rculo
            float x = circleCenter.x + randomRadius * Mathf.Cos(randomAngle);
            float y = circleCenter.y + randomRadius * Mathf.Sin(randomAngle);

            // Creamos un Vector2 con las coordenadas del punto aleatorio
            Vector2 randomPointInCircle = new Vector2(x, y);

            return randomPointInCircle;
        }
        public Vector2 PatrolDirection()
        {
            Vector2 RealDirection = (point - transform.position).normalized;
            switch (-Vector2.SignedAngle(RealDirection, Vector2.right))
            {
                case >= 0 and < 45:
                    return Vector2.right;
                case >= 46 and < 90:
                    return Vector2.right + Vector2.up;
                case >= 91 and < 135:
                    return Vector2.up;
                case >= 136 and < 180:
                    return - Vector2.right + Vector2.up;
                case >= -180 and < -136:
                    return - Vector2.right;
                case >= -135 and < -91:
                    return - Vector2.right - Vector2.up;
                case >= -90 and < -46:
                    return - Vector2.up;
                case >= -45 and < 0:
                    return Vector2.right - Vector2.up;
            }
            return - Vector2.up;
        }
    }
}
