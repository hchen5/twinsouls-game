using Animations;
using enemy;
using FSMState;
using SpecialAtacks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SpecialAtacks.Atacks;

namespace enemy
{
    public class SMEnemyAttackState : MBState
    {
        private SMEnemyBehaviour _Enemy;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        private SpriteRenderer _SpriteRenderer;
        private Transform _TargetTransform;
        [SerializeField]
        private CircleCollider2D _AttackCircleCollider2D;
        //[SerializeField]
        //private float _AttackColliderRange = 2f;
        private bool _PlayerInRangeAttack = false;
        private bool _PlayerInRange = false;
        [SerializeField]
        private AllAtacksName _MyAtackName;
        private Atacks _MyAtack;
        private AnimatorManager _AnimatorManager;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>(); 
            _Animator = GetComponent<Animator>();
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Enemy = GetComponent<SMEnemyBehaviour>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
            _AnimatorManager = new AnimatorManager(GetComponent<Animator>());
        }
        private void Start()
        {
            _MyAtack = NewAtack(gameObject, _MyAtackName);
            _AttackCircleCollider2D.radius = _Enemy.ColliderAttack;
        }
        public override void Init()
        {
            base.Init();
            _TargetTransform = GetComponentInChildren<EnemyFollowRange>().Target;
            _AnimatorManager.SetAnimation(GetComponent<SMEnemyAttackState>().AtackDirection(), AnimatorManager.AimationType.MeleAtack);
            _AttackCircleCollider2D.radius = _Enemy.ColliderAttack;
        }
        public override void Exit()
        {
            base.Exit();
        }
        private void FixedUpdate()
        {
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _TargetTransform = GetComponentInChildren<EnemyFollowRange>().Target;
            _PlayerInRangeAttack = GetComponentInChildren<EnemyAttackRange>().PlayerInRange;
            if (!_PlayerInRangeAttack) 
            {
                if (_PlayerInRange && !_PlayerInRangeAttack) 
                    _StateMachine.ChangeState<SMEnemyFollowState>();
                else
                    _StateMachine.ChangeState<SMEnemyIdleState>();
            }
        }
        public Vector2 AtackDirection()
        {
            if (GetComponentInChildren<EnemyFollowRange>().Target == null)
                return - Vector2.up;
            Vector2 RealDirection = (GetComponentInChildren<EnemyFollowRange>().Target.position - transform.position).normalized;
            switch (-Vector2.SignedAngle(RealDirection, Vector2.right))
            {
                case >= 0 and < 45:
                    return Vector2.right;
                case >= 46 and < 90:
                    return Vector2.right + Vector2.up;
                case >= 91 and < 135:
                    return Vector2.up;
                case >= 136 and < 180:
                    return - Vector2.right + Vector2.up;
                case >= -180 and < -136:
                    return - Vector2.right;
                case >= -135 and < -91:
                    return - Vector2.right - Vector2.up;
                case >= -90 and < -46:
                    return - Vector2.up;
                case >= -45 and < 0:
                    return Vector2.right - Vector2.up;
            }
            return - Vector2.up;
        }
        public void Atack()
        {
            _MyAtack.Atack();
        }
    }
}