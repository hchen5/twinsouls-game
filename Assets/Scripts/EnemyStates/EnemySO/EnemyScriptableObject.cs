using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
[CreateAssetMenu(fileName = "EnemySO" , menuName = "ScriptableOjects/EnemySO")]
public class EnemyScriptableObject : ScriptableObject
{
    public int Damage;
    public int Health;
    public float Velocity;
    public float ColliderAttackRange;
    public float ColliderFollowRange;
    public float ColliderPatrolRange;
    /*
    public AnimationClip AnimationUP;
    public AnimationClip AnimationDown;
    public AnimationClip AnimationLeft;
    public AnimationClip AnimationRigth;
    public AnimationClip AnimationIdle;
    */
    public AnimatorController Animator;
}
