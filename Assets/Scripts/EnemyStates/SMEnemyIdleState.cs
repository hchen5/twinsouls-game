using Animations;
using FSMState;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace enemy
{
    public class SMEnemyIdleState : MBState
    {
        private SMEnemyBehaviour _Enemy;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private NewFiniteStateMachine _StateMachine;
        private SpriteRenderer _SpriteRenderer;
        private bool _PlayerInRange = false;
        private Coroutine _Patroling = null;
        private bool _PlayerInRangeAttack = false;
        private AnimatorManager _AnimatorManager;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Animator = GetComponent<Animator>();
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Enemy = GetComponent<SMEnemyBehaviour>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _AnimatorManager = new AnimatorManager(GetComponent<Animator>());
        }
        public override void Init()
        {
            base.Init();
            _AnimatorManager.SetAnimation(Vector2.zero, AnimatorManager.AimationType.Movement);
        }
        public override void Exit()
        {
            base.Exit();
            _Patroling = null;
        }
        private void FixedUpdate()
        {
            _PlayerInRange = GetComponentInChildren<EnemyFollowRange>().PlayerInRange;
            _PlayerInRangeAttack = GetComponentInChildren<EnemyAttackRange>().PlayerInRange;
            if (_PlayerInRange && !_PlayerInRangeAttack)
            {
                if (_Patroling != null)
                {
                    StopCoroutine(_Patroling);
                }
                _StateMachine.ChangeState<SMEnemyFollowState>();
            }
            else if(_PlayerInRangeAttack)
                _StateMachine.ChangeState<SMEnemyAttackState>();
            else if (_Patroling == null)
                _Patroling = StartCoroutine(WaitToPatroling());
        }
        
        private IEnumerator WaitToPatroling() 
        {
            yield return new WaitForSeconds(2f);
            _StateMachine.ChangeState<SMEnemyPatrolingState>();
        }
    }
}
