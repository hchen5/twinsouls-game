using enemy;
using savegame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CampamentoEnemigo : MonoBehaviour, ISaveableCampament
{
    [SerializeField]
    private int id;
    [SerializeField]
    private EnemyScriptableObject _EnemySOToSpawn;
    [SerializeField]
    private int _NumeroEnemigos;
    [SerializeField]
    private float _CampamentRange;
    private CircleCollider2D _CircleCollider2D;
    private bool _SolvedCamp = false;
    private int _EnemyDead = 0;
    private List<SMEnemyBehaviour> _Enemyspawned;
    private Coroutine _Coroutine;

    public int Id { get => id;}

    private void Awake()
    {
        _CircleCollider2D = GetComponent<CircleCollider2D>();
        _CircleCollider2D.radius = _CampamentRange;
        _Enemyspawned = new List<SMEnemyBehaviour>();
    }
    private void Start()
    {
        if (!_SolvedCamp)
        {
            _Coroutine = StartCoroutine(SpawnEnemies());
            _CircleCollider2D.radius = _CampamentRange;
        }
    }
    private IEnumerator SpawnEnemies()
    {
        yield return new WaitForSeconds(1f);
        if (!_SolvedCamp)
        {
            for (int i = 0; i <_NumeroEnemigos; i++)
            {
                GameObject enemy = PoolManager.Instance.Enemy1Pool.GetElement();
                enemy.transform.position = this.transform.position;
                enemy.GetComponent<SMEnemyPatrolingState>().SetCampamentCollider(_CircleCollider2D);
                enemy.GetComponent<SMEnemyBehaviour>().Init(_EnemySOToSpawn, transform,true);
                _Enemyspawned.Add(enemy.GetComponent<SMEnemyBehaviour>());
            }
            Subs();
        }
    }
    public void CampEnemyDead()
    {
        _EnemyDead += 1;
        if (_EnemyDead == _NumeroEnemigos)
        {
            _SolvedCamp = true;
        }
        DesSubs();
    }
    public SaveData.CampamentoData Save()
    {
        return new SaveData.CampamentoData(_SolvedCamp,id);
    }
    private void Subs()
    {
        if (_Enemyspawned.Count > 0)
        {
            for (int i = 0; i < _Enemyspawned.Count; i++)
            {
                _Enemyspawned[i].GetComponent<SMEnemyBehaviour>().OnEnemyCampDead += CampEnemyDead;
            }
        }
    }
    private void DesSubs() 
    {
        if (_Enemyspawned.Count > 0)
        {
            for (int i = 0; i < _Enemyspawned.Count; i++)
            {
                if (_Enemyspawned[i].GetComponent<SMEnemyBehaviour>().Camp == true)
                {
                    if (_Enemyspawned[i].GetComponent<SMEnemyBehaviour>().Dead1 == true)
                    {
                        _Enemyspawned[i].GetComponent<SMEnemyBehaviour>().OnEnemyCampDead -= CampEnemyDead;
                        _Enemyspawned[i].GetComponent<SMEnemyBehaviour>().Camp = false;
                        _Enemyspawned.Remove(_Enemyspawned[i].GetComponent<SMEnemyBehaviour>());
                    }
                }
            }
        }
    }
    public void Load(SaveData.CampamentoData _CampamentoData)
    {
        _SolvedCamp = _CampamentoData.solved;
        
        if (_Coroutine != null)
           StopCoroutine(_Coroutine);
        if (_Enemyspawned.Count > 0)
        {
            for (int i = 0; i < _Enemyspawned.Count; i++)
            {
                _Enemyspawned[i].GetComponent<Pooleable>().ReturnToPool();
            }
            DesSubs();
            _Enemyspawned.Clear();
        }
            
        if (!_SolvedCamp)
        {
            _Coroutine = StartCoroutine(SpawnEnemies());
            _EnemyDead= 0;
        }
    }
}
