using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AtackManager : MonoBehaviour
{
    [SerializeField]
    private List<AtacksScriptableObjects> _AtackListSerialized;
    private static List<AtacksScriptableObjects> _AtackList;
    void Awake()
    {
        _AtackList = _AtackListSerialized;
    }
    public static List<AtacksScriptableObjects> GetListAtacks()
    {
        return _AtackList;
    }
}
