using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpecialAtacks
{
    public class AtackObjectScript : MonoBehaviour
    {
        [SerializeField]
        private AtacksScriptableObjects _myAtackScriptable;
        public float Damage => _myAtackScriptable.Damage;
        public void Disable() 
        {
            gameObject.SetActive(false);
        }
    }
}

