using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static SpecialAtacks.Atacks;

namespace SpecialAtacks
{
    public class ItemAtack : MonoBehaviour
    {
        [SerializeField]
        private AtacksScriptableObjects _MyAtack;
        public AtacksScriptableObjects MyAtack => _MyAtack;

        public void SetMyAtack(AtacksScriptableObjects Atack)
        {
            _MyAtack = Atack;
            GetComponent<SpriteRenderer>().sprite = _MyAtack.AtackImage;
        }
        void OnEnable()
        {
            if (_MyAtack != null)
                GetComponent<SpriteRenderer>().sprite = _MyAtack.AtackImage;
        }
    }
}

