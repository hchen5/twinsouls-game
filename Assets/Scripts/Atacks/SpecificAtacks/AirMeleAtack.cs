using enemy;
using PlayerNameSpace;
using UnityEngine;

namespace SpecialAtacks
{
    public class AirMeleAtack : Atacks
    {
        public AirMeleAtack(GameObject _AssoceitedGameObject) : base()
        {
            this._AssoceitedGameObject = _AssoceitedGameObject;
            SetMyatackType();
            base.Init();
        }
        public override void SetMyatackType()
        {
            _MyAtackType = AtackType.Mele;
            _MyAtackName = AllAtacksName.AirAtackMele;
        }
        public override void Atack()
        {
            WarnWhenStaminaDrained?.Invoke(_MyAtack.DrainedStamina);
            GameObject atk;
            Vector2 Direction;
            Vector2 Position;
            LayerMask myLayer;
            if (_AssoceitedGameObject.name.Contains("Player"))
            {
                atk = _AssoceitedGameObject.GetComponent<PlayerAtacks>().MyAtacksPoolManager.GetComponent<Atacks_PoolManager>().EnableAtack(MyAtackName);
                Direction = _AssoceitedGameObject.GetComponent<SM_PlayerMovement>().LookingToDirection;
                Position = _AssoceitedGameObject.transform.position;
                myLayer = LayerMask.NameToLayer("PlayerHitBox");
            }
            else
            {
                if (_AssoceitedGameObject.GetComponentInChildren<EnemyFollowRange>().Target == null)
                    return;
                atk = _AssoceitedGameObject.GetComponentInChildren<EnemyFollowRange>().Target.GetComponent<PlayerAtacks>().MyAtacksPoolManager.GetComponent<Atacks_PoolManager>().EnableAtack(MyAtackName);
                Direction = _AssoceitedGameObject.GetComponent<SMEnemyAttackState>().AtackDirection();
                Position = _AssoceitedGameObject.transform.position;
                myLayer = LayerMask.NameToLayer("EnemyHitBox");
            }
            if (Direction == Vector2.zero)
                Direction = Vector2.down;
            Vector2 SpawnPoint = Position + Direction * 1;
            atk.transform.position = SpawnPoint;
            atk.transform.right = Direction;
            atk.layer = myLayer;
            atk.GetComponent<Rigidbody2D>().velocity = Direction * MyAtack.velocity;
            atk.GetComponent<Animator>().Play("Atack");
        }
    }
}
