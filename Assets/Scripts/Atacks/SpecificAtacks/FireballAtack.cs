using enemy;
using PlayerNameSpace;
using UnityEngine;

namespace SpecialAtacks
{
    public class FireballAtack : Atacks
    {
        public FireballAtack(GameObject _AssoceitedGameObject)
        {
            this._AssoceitedGameObject = _AssoceitedGameObject;
            SetMyatackType();
            base.Init();
        }
        public override void SetMyatackType()
        {
            _MyAtackType = AtackType.Spell;
            _MyAtackName = AllAtacksName.FireballAtack;
        }
        public override void Atack()
        {
            WarnWhenManaDrained?.Invoke(_MyAtack.DrainedMana);
            GameObject atk;
            Vector2 Direction;
            Vector2 Position;
            LayerMask myLayer;
            Atacks_PoolManager PoolaManager;
            if (_AssoceitedGameObject.name.Contains("Player"))
            {
                PoolaManager = _AssoceitedGameObject.GetComponent<PlayerAtacks>().MyAtacksPoolManager.GetComponent<Atacks_PoolManager>();
                Direction = _AssoceitedGameObject.GetComponent<SM_PlayerMovement>().LookingToDirection;
                Position = _AssoceitedGameObject.transform.position;
                myLayer = LayerMask.NameToLayer("PlayerHitBox");
            }
            else
            {
                if (_AssoceitedGameObject.GetComponentInChildren<EnemyFollowRange>().Target == null)
                    return;
                PoolaManager = _AssoceitedGameObject.GetComponentInChildren<EnemyFollowRange>().Target.GetComponent<PlayerAtacks>().MyAtacksPoolManager.GetComponent<Atacks_PoolManager>();
                Direction = _AssoceitedGameObject.GetComponent<SMEnemyAttackState>().AtackDirection();
                Position = _AssoceitedGameObject.transform.position;
                myLayer = LayerMask.NameToLayer("EnemyHitBox");
            }
            if (Direction == Vector2.zero)
                Direction = Vector2.down;
            for (int i = -1; i <= 1; i++)
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0)
                        continue;
                    atk = PoolaManager.EnableAtack(MyAtackName);
                    Vector2 SpawnPoint = Position + new Vector2(i, j) * 0.1f + Direction * 2;
                    atk.transform.position = SpawnPoint;
                    atk.transform.right = new Vector2(i, j);
                    atk.layer = myLayer;
                    if (i == -1 && j == 0)
                        atk.transform.up = - atk.transform.up;
                    //atk.GetComponent<Rigidbody2D>().velocity = new Vector2(i, j) * MyAtack.velocity;
                    atk.GetComponent<AtackFireBallParabolic>().UpdateX(Direction);
                    atk.GetComponent<Animator>().Play("Atack");
                }
        }
    }
}
