using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AtackFireBallParabolic : MonoBehaviour
{
    Vector3 _InitialPoint;
    Vector2 Direction;
    public void Init()
    {
        _InitialPoint = transform.position;
    }
    public void UpdateX(Vector2 D)
    {
        Direction = D;
        StartCoroutine(UpdateRutine());
    }
    private IEnumerator UpdateRutine()
    {
        while(true)
        {
            GetComponent<Rigidbody2D>().velocity = TirParabolic(_InitialPoint + transform.up, _InitialPoint, 0.5f);
            yield return new WaitForSeconds(.1f);
        }
    }
    private Vector2 TirParabolic(Vector3 TargetPos, Vector3 InitialPos, float Time) 
    {
        Vector2 objectivepos = TargetPos - InitialPos;
        float InitialVelocityX =  (objectivepos.x) / Time + (Direction.x * Time) /2f;
        float InitialVelocityY = (objectivepos.y) / Time + (Direction.y * Time) /2f;

        return new Vector2 (InitialVelocityX, InitialVelocityY);
    }
    public void Exit()
    {
        StopAllCoroutines();
    }
}
