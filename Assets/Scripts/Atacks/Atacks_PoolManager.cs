using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SpecialAtacks.Atacks;

namespace SpecialAtacks
{
    public class Atacks_PoolManager : MonoBehaviour
    {
        public GameObject EnableAtack(AllAtacksName Name)
        {
            return transform.Find(Enum.GetName(typeof(AllAtacksName), Name)).GetComponent<Pool>().GetElement();
        }
    }
}
