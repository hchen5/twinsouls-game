using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PlayerNameSpace;
using UnityEngine;

namespace SpecialAtacks
{
    public abstract class Atacks
    {
        // All Childs of Atack, Remember to put in enum the Atack Name (same as function name) when created
        public enum AllAtacksName { PoweredMeleAtack, FireballAtack, AirAtackMele}
        public enum AtackType { Mele, Spell }
        protected AtackType _MyAtackType;
        protected AllAtacksName _MyAtackName;
        protected AtacksScriptableObjects _MyAtack;
        public AtacksScriptableObjects MyAtack => _MyAtack;
        protected static Dictionary<AllAtacksName, AtacksScriptableObjects> _AllAtacksScriptables;
        protected GameObject _AssoceitedGameObject;
        protected Coroutine _MyCoolDownCoroutine;
        protected int? _MyPosition;
        public AtackType MyAtackType { get { return _MyAtackType; }}	
        public AllAtacksName MyAtackName { get { return _MyAtackName; }}
        private List<AtacksScriptableObjects> _AllScrptableObjectsAtacks = AtackManager.GetListAtacks();
        public delegate void Warn(float a);
        public Warn WarnWhenStaminaDrained;
        public Warn WarnWhenHealthDrained;
        public Warn WarnWhenManaDrained;

        public void Init()
        {
            if (_AllAtacksScriptables == null)
                SetDictionaryNameScriptableObject();
            _MyAtack = _AllAtacksScriptables[_MyAtackName];
        }
        /// <summary>
        /// Do my class Atack
        /// </summary>
        public abstract void Atack();
        /// <summary>
        /// Set _MyAtackType and _MyAtackName, OBLIGATORY!!
        /// </summary>
        public abstract void SetMyatackType();
        // Get the specific COOlDown Time
        private float GetCoolDownAtack()
        {   
            return _MyAtack.CoolDownTime;
        }
        public Sprite GetMyAtackSprite()
        {   
            return _MyAtack.AtackImage;
        }
        public void SetDictionaryNameScriptableObject()
        {
            _AllAtacksScriptables = new Dictionary<AllAtacksName, AtacksScriptableObjects>();
            foreach (AtacksScriptableObjects atk in _AllScrptableObjectsAtacks)
                _AllAtacksScriptables.Add(atk.Atack, atk);
        }
        // CoolDown Coroutine
        private IEnumerator CoolDownCoroutine(float Number)
        {
            if (_MyPosition == null)
                SetMyPosition();
            float MaxVal = Number;
            while (Number > 0)
            {
                float Val = Number/MaxVal;
                _AssoceitedGameObject.GetComponent<PlayerUI>().UpdateCoolDownAtacks((int)_MyPosition,Val,(int)Number);
                yield return new WaitForSeconds(.2f);
                Number -= .2f;
            }
            _AssoceitedGameObject.GetComponent<PlayerUI>().UpdateCoolDownAtacks((int)_MyPosition,0,0);
            _MyCoolDownCoroutine = null;
        }
        private void SetMyPosition()
        {
            _MyPosition = _AssoceitedGameObject.GetComponent<PlayerAtacks>().SpecificAtackInput-1;
        }
        /// <summary>
        /// Check if can make the atack and if is so, begins the coolDown Coroutine
        /// </summary>	
        public bool SetCoolDown()
        {
            if (_MyCoolDownCoroutine == null)
            {
                _MyCoolDownCoroutine = _AssoceitedGameObject.GetComponent<PlayerAtacks>().StartCoroutine(CoolDownCoroutine(GetCoolDownAtack()));
                return false;
            }
            else
                return true;
        }
        public bool CoolDown()
        {
            if (_MyCoolDownCoroutine == null)
                return false;
            else
                return true;
        }
        /// <summary>
        /// Factory of Atacks
        /// </summary>	
        public static Atacks NewAtack(GameObject Character, AllAtacksName NameAtack)
        {
            switch (NameAtack) 
            {
                case AllAtacksName.PoweredMeleAtack:
                    return new PoweredMeleAtack(Character);
                case AllAtacksName.FireballAtack:
                    return new FireballAtack(Character);
                case AllAtacksName.AirAtackMele:
                    return new AirMeleAtack(Character);
                default:
                    Debug.LogError("Atack not added to NewAtack Function (The Factroy)");
                    return null;
            }
        }
    }
}

