using savegame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance => m_Instance;
    public bool LoadScene { get => _LoadScene;}

    private bool _LoadScene = false;
    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }
    public void StartGame()
    {
        _LoadScene = false;
        SceneManager.LoadScene("SampleScene");
    }
    public void LoadGame() 
    {
        _LoadScene = true;
        SceneManager.LoadScene("SampleScene");
    }
    public void MenuScene() 
    {
        SceneManager.LoadScene("StartMenu");
    }
}
