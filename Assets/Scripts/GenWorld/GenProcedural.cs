using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GenProcedural : MonoBehaviour
{
    [SerializeField]
    private Tilemap tilemap;
    [SerializeField]
    private Tile[] tiles;
    [SerializeField]
    private float[] LevelsTiles;
    [SerializeField]
    private GameObject[] Objects;
    [SerializeField]
    private float[] LevelsObjects;
    private float[,] m_Terrain;
    [SerializeField]
    private Tilemap tilemapcorners;
    [SerializeField]
    private Tile[] tilesCorner;
    public void Start()
    {
        if (Check())
            StartCoroutine(WaitGenerator());
    }
    private bool Check()
    {
        if (tiles.Length != LevelsTiles.Length)
        {
            Debug.LogError("Las Listas de los Tiles No Coinciden!!");
            return false;
        }
        if (Objects.Length != LevelsObjects.Length)
        {
            Debug.LogError("Las Listas de los Objects No Coinciden!!");
            return false;
        }
        return true;
    }
    private void DrawAllTerrain()
    {
        for (int i = m_Terrain.GetLength(0)-1; i >= 0; i--)
            for (int j = m_Terrain.GetLength(1)-1; j >= 0; j--)
            {
                bool check = GetComponent<GenGameObjectExtra>().GenObjectExtra(m_Terrain[j, i], new Vector3Int(j, i, 0));
                check = check || GetComponent<GenChests>().GenObjectExtra(m_Terrain[j, i], new Vector3Int(j, i, 0));
                check = check || GetComponent<GenCampamentos>().GenObjectExtra(m_Terrain[j, i], new Vector3Int(j, i, 0));
                tilemap.SetTile(new Vector3Int(j, i, 0), SelectTile(m_Terrain[j, i]));
                GameObject obj = selctObject(GetComponent<CreatePerlin>().GetValueOfPerlin(j, i));
                if (!check && obj != null)
                {
                    obj.transform.position = new Vector3Int(j, i, 0);
                    Instantiate(selctObject(GetComponent<CreatePerlin>().GetValueOfPerlin(j, i)), transform);
                }
            }
    }
    private void DrawTile(int j, int i)
    {
        tilemap.SetTile(new Vector3Int(j, i, 0), SelectTile(m_Terrain[j, i]));
        GameObject obj = selctObject(GetComponent<CreatePerlin>().GetValueOfPerlin(j, i));
        if (obj)
        {
            Instantiate(obj, transform);
            obj.transform.position = tilemap.GetCellCenterWorld(new Vector3Int(j, i, 0));
        }
    }

    private Tile SelectTile(float p)
    {
        for (int i = 0; i < LevelsTiles.Length; i++)
        {
            if (p < LevelsTiles[i]) return tiles[i];
        }
        return null;
    }
    
    private GameObject selctObject(float p)
    {
        for (int i = 0; i < LevelsObjects.Length; i++)
        {
            if (p < LevelsObjects[i]) return Objects[i];
        }
        return null;
    }
    private IEnumerator WaitGenerator()
    {
        while (m_Terrain == null)
        {
            m_Terrain = GetComponent<CreatePerlin>().GetPerlin();
            yield return new WaitForSeconds(1);
        }
        DrawAllTerrain();
        DrawAllCorners();
    }
    
    private void DrawAllCorners()
    {
        for (int i = m_Terrain.GetLength(0)-9; i >= 8; i--)
        {
            tilemapcorners.SetTile(new Vector3Int(m_Terrain.GetLength(1)-9, i, 0), tilesCorner[0]);
            tilemapcorners.SetTile(new Vector3Int(7, i, 0), tilesCorner[1]);
        }
        for (int j = m_Terrain.GetLength(1)-9; j >= 8; j--)
        {
            tilemapcorners.SetTile(new Vector3Int(j, m_Terrain.GetLength(0)-9, 0), tilesCorner[2]);
            tilemapcorners.SetTile(new Vector3Int(j, 7, 0), tilesCorner[3]);
        }
    }
}
