using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenCampamentos : MonoBehaviour
{
    [SerializeField]
    private PoolPocho _MyPoolObject;
    [SerializeField]
    private float MinRangePerlin;
    [SerializeField]
    private float MaxRangePerlin;

    public bool GenObjectExtra(float p, Vector3 pos)
    {
        if (p >= MinRangePerlin && p < MaxRangePerlin)
        {
            GameObject obj = _MyPoolObject.GetElement();
            if (obj == null)
                    return false;
            obj.transform.position = pos;
            return true;
        }
        return false;
    }
}
