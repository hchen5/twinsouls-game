using enemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private float _SpawnTime = 5f;
    [SerializeField]
    private Transform[] _SpawnPoint;
    [SerializeField]
    private EnemyScriptableObject[] _EnemySOs;
    private Coroutine _Coroutine;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") 
        {
            _Coroutine = StartCoroutine(Spawnear());
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (_Coroutine != null)
                StopCoroutine(_Coroutine);
        }
    }
    private IEnumerator Spawnear() 
    {
        while (true) 
        {
            Transform RandomTransform = _SpawnPoint[Random.Range(0, _SpawnPoint.Length)];
            EnemyScriptableObject RandomEnemySO = _EnemySOs[Random.Range(0, _EnemySOs.Length)];
            GameObject g = PoolManager.Instance.Enemy1Pool.GetElement();
            if (g != null)
                g.GetComponent<SMEnemyBehaviour>().Init(RandomEnemySO, RandomTransform, false);
            yield return new WaitForSeconds(_SpawnTime);
            
        }
    }
}
