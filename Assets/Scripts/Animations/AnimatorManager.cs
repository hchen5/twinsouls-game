using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Animations
{
    public class AnimatorManager
    {
        private Animator Animator;
        /// <summary>
        /// AnimationType of Characters Objects
        /// </summary>
        public enum AimationType { Movement, BasicAtack, MeleAtack, SpellAtack }
        /// <summary>
        /// Constructor to initialize an Animator Manager for an Object
        /// </summary>
        /// <param name="Animator"> Te Animator Component Atached</param>
        public AnimatorManager(Animator Animator)
        {
            this.Animator = Animator;
        }
        /// <summary>
        /// Set Respective Animation
        /// </summary>
        public void SetAnimation(Vector2 Direction, AimationType AnimType)
        {
            if (AnimType == AimationType.Movement)
                SetAnimationMovement(Direction);
            if (AnimType == AimationType.BasicAtack)
                SetAnimationBasicAtack(Direction);
            if (AnimType == AimationType.MeleAtack)
                SetAnimationMeleAtack(Direction);
            if (AnimType == AimationType.SpellAtack)
                SetAnimationSpellAtack(Direction);
        }
        /// <summary>
        /// Get Current Animation Name
        /// </summary>
        public string GetCurrentAnimation()
        {
            string AnimationName;
            AnimationName = Animator.GetCurrentAnimatorClipInfo(0)[0].clip.name;
            return AnimationName;
        }
        private void SetAnimationMovement(Vector2 Direction)
        {
            if (Direction == Vector2.up)
                Animator.Play("Move_Up");
            else if (Direction == Vector2.down)
                Animator.Play("Move_Down");
            else if (Direction == Vector2.left)
                Animator.Play("Move_Left");
            else if (Direction == Vector2.right)
                Animator.Play("Move_Right");
            else if (Direction == Vector2.zero)
                Animator.Play("Iddle");
            else
                SetAnimationMovement(GetLast4DDirection());
        }
        private void SetAnimationBasicAtack(Vector2 Direction)
        {
            if (Direction == Vector2.up)
                Animator.Play("AtackBasic_Up");
            else if (Direction == Vector2.down)
                Animator.Play("AtackBasic_Down");
            else if (Direction == Vector2.left)
                Animator.Play("AtackBasic_Left");
            else if (Direction == Vector2.right)
                Animator.Play("AtackBasic_Right");
            else if (Direction == Vector2.zero)
                Animator.Play("AtackBasic_Down");
            else
                SetAnimationBasicAtack(GetLast4DDirection());
        }
        private void SetAnimationMeleAtack(Vector2 Direction)
        {
            if (Direction == Vector2.up)
                Animator.Play("AtackMele_Up");
            else if (Direction == Vector2.down)
                Animator.Play("AtackMele_Down");
            else if (Direction == Vector2.left)
                Animator.Play("AtackMele_Left");
            else if (Direction == Vector2.right)
                Animator.Play("AtackMele_Right");
            else if (Direction == Vector2.zero)
                Animator.Play("AtackMele_Down");
            else
                SetAnimationMeleAtack(GetLast4DDirection());
        }
        private void SetAnimationSpellAtack(Vector2 Direction)
        {
            if (Direction == Vector2.up)
                Animator.Play("AtackSpell_Up");
            else if (Direction == Vector2.down)
                Animator.Play("AtackSpell_Down");
            else if (Direction == Vector2.left)
                Animator.Play("AtackSpell_Left");
            else if (Direction == Vector2.right)
                Animator.Play("AtackSpell_Right");
            else if (Direction == Vector2.zero)
                Animator.Play("AtackSpell_Down");
            else
                SetAnimationSpellAtack(GetLast4DDirection());
        }
        // Get Las 4D Direction By Last Animation
        private Vector2 GetLast4DDirection()
        {
            Vector2 Direction;
            string CurrentClipName = GetCurrentAnimation();
            if (CurrentClipName.Split("_").Length == 1)
            {
                Direction = Vector2.down;
                return Direction;
            }
            if (CurrentClipName.Split("_")[1] == "Up")
                Direction = Vector2.up;
            else if (CurrentClipName.Split("_")[1] == "Down")
                Direction = Vector2.down;
            else if (CurrentClipName.Split("_")[1] == "Left")
                Direction = Vector2.left;
            else if (CurrentClipName.Split("_")[1] == "Right")
                Direction = Vector2.right;
            else
                Direction = Vector2.down;
            return Direction;
        }
    }
}
