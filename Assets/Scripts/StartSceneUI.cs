using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartSceneUI : MonoBehaviour
{
    public void StartGame() 
    {
        GameManager.Instance.StartGame();
    }
    public void LoadGame() 
    {
        GameManager.Instance.LoadGame();
    }
}
