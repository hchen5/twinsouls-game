using PlayerNameSpace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using World;
using static savegame.SaveData;

namespace savegame
{
    public class SaveDataManager : MonoBehaviour
    {
        private const string saveFileName = "savegame.json";
        private static SaveDataManager m_Instance;
        public static SaveDataManager Instance => m_Instance;
        public void Start()
        {
            if (GameManager.Instance.LoadScene)
                LoadData();
        }
        IEnumerator WaitToLoad() 
        {
            string jsonData = File.ReadAllText(saveFileName);

            SaveData data = new SaveData();
            JsonUtility.FromJsonOverwrite(jsonData, data);
            yield return new WaitForSeconds(1.5f);
            Chest[] box = FindObjectsByType<Chest>(FindObjectsSortMode.None);
            for (int j = 0; j < box.Length; j++)
            {
                for (int h = 0; h < box.Length; h++)
                {
                    //box[j].Load(data._Boxes[j]);
                    if (box[j].Id == data._Boxes[h].id)
                    {
                        box[j].Load(data._Boxes[h]);
                    }
                }
            }
            CampamentoEnemigo[] camp = FindObjectsByType<CampamentoEnemigo>(FindObjectsSortMode.None);
            for (int j = 0; j < camp.Length; j++)
            {
                for (int h = 0; h < camp.Length; h++)
                {
                    if (camp[j].Id == data._Camps[h].id)
                    {
                        camp[j].Load(data._Camps[h]);
                    }
                }
            }
        }
        public void SaveData()
        {
            ISaveablePlayer saveablePlayer = FindObjectOfType<PlayerNameSpace.Player>();
            ISaveableCampament[] saveableCampament = FindObjectsByType<CampamentoEnemigo>(FindObjectsSortMode.None);
            ISaveableBoxes[] saveableBoxes = FindObjectsByType<Chest>(FindObjectsSortMode.None);
            ISaveableInventory saveableInventory = FindObjectOfType<PlayerInventory>();

            SaveData data = new SaveData();
            data.SavePlayerData(saveablePlayer);
            data.SaveCampamentoData(saveableCampament);
            data.SaveBoxData(saveableBoxes);
            data.SaveInventoryData(saveableInventory);
            string jsonData = JsonUtility.ToJson(data);
            try
            {
                Debug.Log("Saving: ");
                Debug.Log(jsonData);

                File.WriteAllText(saveFileName, jsonData);
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to save {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }
        public void LoadData()
        {
            try
            {
                string jsonData = File.ReadAllText(saveFileName);

                SaveData data = new SaveData();
                JsonUtility.FromJsonOverwrite(jsonData, data);

                PlayerInventory PI = FindObjectOfType<PlayerInventory>();
                PI.Load(data._InventoryData);

                PlayerNameSpace.Player player = FindObjectOfType<PlayerNameSpace.Player>();
                player.Load(data._Player);

                StartCoroutine(WaitToLoad());

                /*
                enemy.SMEnemyBehaviour[] enemies = FindObjectsByType<enemy.SMEnemyBehaviour>(FindObjectsSortMode.None);
                for (int i = 0; i < enemies.Length; i++)
                    enemies[i].Load(data._Enemies[i]);*/
            }
            catch (Exception e)
            {
                Debug.LogError($"Error while trying to load {Path.Combine(Application.persistentDataPath, saveFileName)} with exception {e}");
            }
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.G))
                SaveData();
        }

    }
}