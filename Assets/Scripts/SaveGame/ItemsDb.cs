using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[CreateAssetMenu(fileName = "ItemDb", menuName = "ScriptableObjects/ItemDb")]
public class ItemsDb : ScriptableObject
{
    public List<ItemScriptableObject> items;
    public ItemScriptableObject GetItem(int id)
    {
        return items.FirstOrDefault(item => item.id == id);
    }
}
