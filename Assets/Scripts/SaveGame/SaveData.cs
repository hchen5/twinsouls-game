using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static savegame.SaveData;
using static SpecialAtacks.Atacks;

namespace savegame
{
    [Serializable]
    public class SaveData 
    {
        [Serializable]
        public struct PlayerData
        {
            public Vector3 position;
            public int vida;
            public PlayerData(Vector3 _position, int vid)
            {
                position = _position;
                vida = vid;
            }
        }
        public void SavePlayerData(ISaveablePlayer _playerData)
        {
            _Player = _playerData.Save();
        }
        /*Solo si queremos guardar enemigos
        [Serializable]
        public struct EnemyData
        {
            public Vector3 position;

            public EnemyData(Vector3 _position)
            {
                position = _position;
            }
        }
        public void SaveEnemyData(ISaveableEnemy[] _enemyData) 
        {
            _Enemies = new EnemyData[_enemyData.Length];
            for(int i = 0; i < _enemyData.Length; i++)
                _Enemies[i] = _enemyData[i].Save();
        }*/
        [Serializable]
        public struct CampamentoData 
        {
            public bool solved;
            public int id;
            public CampamentoData(bool derrotats ,int ids) 
            {
                id = ids;
                solved = derrotats;
            }
        }
        public void SaveCampamentoData(ISaveableCampament[] _campamentos)
        {
            _Camps = new CampamentoData[_campamentos.Length];
            for (int i = 0; i < _campamentos.Length; i++)
                _Camps[i] = _campamentos[i].Save();
        }
        [Serializable]
        public struct BoxData 
        {
            public bool opened;
            public int id;
            public BoxData(bool openedbox, int idb) 
            {
                opened = openedbox;
                id = idb;
            }
        }
        public void SaveBoxData(ISaveableBoxes[] _Boxesf) 
        {
            _Boxes = new BoxData[_Boxesf.Length];
            for (int i = 0; i < _Boxesf.Length; i++)
                _Boxes[i] = _Boxesf[i].Save();
        }
        [Serializable]
        public struct InventoryData 
        {
            public List<int> itemsid;
            public InventoryData(List<int> inventory) 
            {
                itemsid = inventory;
            }
        }
        public void SaveInventoryData(ISaveableInventory _InventoryD) 
        {
            _InventoryData = _InventoryD.Save();   
        }
        [Serializable]
        public struct AttacksData
        {
            public List<AllAtacksName> atacks;
            public AttacksData(List<AllAtacksName> atackss)
            {
                atacks = atackss;
            }
        }
        public void SaveAttacksData(ISaveableAttacks _Attacks)
        {
            _AttacksData = _Attacks.Save();
        }
        /*
        [Serializable]
        public struct SpawnersData
        {
            public Vector2 position;
            public SpawnersData(Vector3 sPositon)
            { 
                position = sPositon;
            }
        }
        public void SaveSpawnersData(ISaveableBoxes[] _Boxesf)
        {
            _Boxes = new BoxData[_Boxesf.Length];
            for (int i = 0; i < _Boxesf.Length; i++)
                _Boxes[i] = _Boxesf[i].Save();
        }*/
        public PlayerData _Player;
        public CampamentoData[] _Camps;
        public BoxData[] _Boxes;
        public InventoryData _InventoryData;
        public AttacksData _AttacksData;
        //public SpawnersData[] _Spawners;
        /*Solo si queremos guardar enemigos
        public EnemyData[] _Enemies;
        */
    }
    //-----------------------------------Interfaces-----------------------------------------------//
    public interface ISaveablePlayer
    {
        public PlayerData Save();
        public void Load(PlayerData _playerdata);
    }
    public interface ISaveableCampament
    {
        public CampamentoData Save();
        public void Load(CampamentoData _CampamentoData);
    }
    public interface ISaveableBoxes 
    {
        public BoxData Save();
        public void Load(BoxData _BoxesData);
    }
    public interface ISaveableInventory 
    {
        public InventoryData Save();
        public void Load(InventoryData _InventoryData);
    }
    public interface ISaveableAttacks
    {
        public AttacksData Save();
        public void Load(AttacksData _AttacksData);
    }
    /*
    public interface ISaveableSapwners 
    {
        public SpawnersData Save();
        public void Load(SpawnersData _Spawners);
    }*/
    /*Solo si queremos guardar enemigos
    public interface ISaveableEnemy
    {
        public EnemyData Save();
        public void Load(EnemyData _enemyData);
    }*/
}