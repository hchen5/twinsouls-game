using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneFinalUI : MonoBehaviour
{
    public void LoadEscenaMenu() 
    {
        SceneManager.LoadScene("StartMenu");
    }
}
