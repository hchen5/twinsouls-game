using System;
using System.Collections;
using System.Collections.Generic;
using PlayerNameSpace;
using savegame;
using UnityEngine;

namespace World
{
    public class Chest : WorldInteractable, ISaveableBoxes
    {
        [SerializeField]
        private int id;
        [SerializeField]
        private List<ChestProbabilities> _MyProbabilities;
        [SerializeField]
        private int _ObjectCuantity;
        [SerializeField]
        private float _SpawnDistance;
        private bool _Opened = false;

        public int Id { get => id;}

        public override void Interact(GameObject gameObject)
        {
            for (int i = 0; i < _ObjectCuantity; i++)
            {
                ScriptableObject item = Randomizer(_MyProbabilities);
                Vector3 Vini = transform.up;
                Vector3 Vfin = Quaternion.AngleAxis(360/(i + 1) + 90 , Vector3.forward) * Vini.normalized;
                Vector3 Position = transform.position + Vfin.normalized * _SpawnDistance;
                Debug.Log(Vfin);
                if (item.GetType().Equals(typeof(ItemScriptableObject)))
                    gameObject.GetComponent<PlayerInventory>().SpawnItemAtack((ItemScriptableObject)item, Position);
                else if (item.GetType().Equals(typeof(AtacksScriptableObjects)))
                    gameObject.GetComponent<PlayerAtacks>().SpawnItemAtack((AtacksScriptableObjects)item, Position);
            }
            this.gameObject.GetComponent<Animator>().Play("Open");
            this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
            _Opened = true;
        }

        public void Load(SaveData.BoxData _BoxesData)
        {
            if(_BoxesData.opened)
            {
                this.gameObject.layer = LayerMask.NameToLayer("OpenedBox");
                this.gameObject.tag = "Untagged";
                this.gameObject.GetComponent<Animator>().Play("Opened");
            }
        }

        public SaveData.BoxData Save()
        {
            return new SaveData.BoxData(_Opened,Id);
        }

        [Serializable]
        public struct ChestProbabilities
        {
            [SerializeField]
            public ScriptableObject MyScriptable;
            [SerializeField]
            public int Probabilitie;
        }
    }
}

