using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static World.Chest;
using Random = UnityEngine.Random;

namespace World
{
    public abstract class WorldInteractable : MonoBehaviour
    {
        public abstract void Interact(GameObject gameObject);
        protected ScriptableObject Randomizer(List<ChestProbabilities> _MyProbabilities)
        {
            ScriptableObject sco = null;
            int rd = Random.Range(0,100);
            _MyProbabilities = _MyProbabilities.OrderBy(x=> Random.Range(0,1000)).ToList();
            foreach (ChestProbabilities p in _MyProbabilities)
            {
                if (p.Probabilitie >= rd || p.Probabilitie < rd)
                    sco = p.MyScriptable;
            }
            return sco;
        }
    }
}


