using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Items
{
    public class ItemObject : MonoBehaviour
    {
        [SerializeField]
        private ItemScriptableObject _myItem;
        public ItemScriptableObject myItem => _myItem;
        public void SetMyItem(ItemScriptableObject item)
        {
            _myItem = item;
            GetComponent<SpriteRenderer>().sprite = _myItem.ItemImage;
        }
    }
}

