using System.Collections;
using System.Collections.Generic;
using System.Threading;
using EconomyManager;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace PlayerNameSpace
{
    public class PlayerUI : MonoBehaviour
    {
        public enum UiItem {HealthBar, StaminaBar, ManaBar}
        [Header("UI")]
        [SerializeField]
        private GameObject _HealthSlider;
        [SerializeField]
        private GameObject _ManaSlider;
        [SerializeField]
        private GameObject _StaminaSlider;
        [SerializeField]
        private GameObject[] _SlotsAtacks;
        [SerializeField]
        private GameObject[] _SlotsItems;

        void Start()
        {
            setUIValues();
        }
        private void setUIValues()
        {
            UpdateUIEconomyMaxValues(UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.MaxValue);
            UpdateUIEconomyMinValues(UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.MinValue);
            UpdateUIEconomyValues(UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.Value);
            UpdateUIEconomyMaxValues(UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.MaxValue);
            UpdateUIEconomyMinValues(UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.MinValue);
            UpdateUIEconomyValues(UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.Value);
            UpdateUIEconomyMaxValues(UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.MaxValue);
            UpdateUIEconomyMinValues(UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.MinValue);
            UpdateUIEconomyValues(UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.Value);
        }

        public void UpdateUIEconomyValues(UiItem item, float num)
        {
            if (item == UiItem.HealthBar)
                _HealthSlider.GetComponent<Slider>().value = num;
            if (item == UiItem.ManaBar)
                _ManaSlider.GetComponent<Slider>().value = num;
            if (item == UiItem.StaminaBar)
                _StaminaSlider.GetComponent<Slider>().value = num;
        }
        public void UpdateUIEconomyMaxValues(UiItem item, float num)
        {
            if (item == UiItem.HealthBar)
                _HealthSlider.GetComponent<Slider>().maxValue = num;
            if (item == UiItem.ManaBar)
                _ManaSlider.GetComponent<Slider>().maxValue = num;
            if (item == UiItem.StaminaBar)
                _StaminaSlider.GetComponent<Slider>().maxValue = num;
        }
        public void UpdateUIEconomyMinValues(UiItem item, float num)
        {
            if (item == UiItem.HealthBar)
                _HealthSlider.GetComponent<Slider>().minValue = num;
            if (item == UiItem.ManaBar)
                _ManaSlider.GetComponent<Slider>().minValue = num;
            if (item == UiItem.StaminaBar)
                _StaminaSlider.GetComponent<Slider>().minValue = num;
        }
        public void UpdateCoolDownAtacks(int position, float val, int sec)
        {
            _SlotsAtacks[position].transform.Find("BluryImage").GetComponent<Image>().fillAmount = val;
            if (sec != 0)
                _SlotsAtacks[position].transform.Find("Text").GetComponent<TextMeshPro>().text = sec.ToString();
            else
                _SlotsAtacks[position].transform.Find("Text").GetComponent<TextMeshPro>().text = "";
        }
        public void UpdateAtackSprite(int position, Sprite img)
        {
            if (_SlotsAtacks.Length > position)
                _SlotsAtacks[position].GetComponent<Image>().sprite = img;
            if (img == null)
                _SlotsAtacks[position].GetComponent<Image>().color = new Color(1,1,1,0);
            else
                _SlotsAtacks[position].GetComponent<Image>().color = new Color(1,1,1,1);
        }
        public void UpdateItemSprite(int position, Sprite img)
        {
            if (_SlotsAtacks.Length > position)
                _SlotsItems[position].GetComponent<Image>().sprite = img;
            if (img == null)
                _SlotsItems[position].GetComponent<Image>().color = new Color(1,1,1,0);
            else
                _SlotsItems[position].GetComponent<Image>().color = new Color(1,1,1,1);
        }
    }
}
