using System;
using System.Linq;
using Animations;
using EconomyManager;
using FSMState;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static Animations.AnimatorManager;
using static PlayerNameSpace.PlayerUI;

namespace PlayerNameSpace
{
    public class SM_PlayerMovement : MBState
    {
        // GameObject Components
        private Rigidbody2D _Rigidbody2D;
        private Animator _Animator;
        // GameObject Variables
        private int _Velocity;
        private float _Dash;
        private float _CurrentVelocty;
        // Informative Variables
        private Vector2 _Direction;
        public Vector2 LookingToDirection { get { return _Direction; } }
        private float _SpecificAtackInput;
        public float SpecificAtackInput { get { return _SpecificAtackInput; } }
        // Manager Variables
        private AnimatorManager _AnimatorManager;
        // Ecocmoy Variables
        private Economy _Stamina;
        private float _DrainedStaminaPerSeconds;
        private float _GainedStaminaPerSeconds;
        private PlayerUI _UIManager;
        // new
        private NewFiniteStateMachine _StateMachine;
        [SerializeField]
        private InputActionAsset _InputActionAsset;
        internal InputActionMap _Player1;
        internal InputActionMap _Player2;

        private void Awake()
        {
            _Player1 = _InputActionAsset.FindActionMap("PlayerOne");
            _Player2 = _InputActionAsset.FindActionMap("PlayerTwo");
            _Rigidbody2D = GetComponent<Rigidbody2D>();
            _Velocity = GetComponent<Player>().Velocity;
            _Dash = GetComponent<Player>().Dash;
            _Stamina = GetComponent<PlayerEconomy>()._Stamina;
            _DrainedStaminaPerSeconds = GetComponent<PlayerEconomy>().DrainedStaminaPerSeconds;
            _GainedStaminaPerSeconds = GetComponent<PlayerEconomy>().GainedStaminaPerSeconds;
            _UIManager = GetComponent<PlayerUI>();
            _Animator = GetComponent<Animator>();
            _AnimatorManager = new AnimatorManager(_Animator);
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            _Player1.Enable();
            _Player2.Enable();
        }
        public override void Init()
        {
            _Rigidbody2D.velocity = Vector2.zero;
            _Stamina.WarnWhenMinValue += StaminaWhenMinValue;
            _Stamina.WarnWhenMaxValue += StaminaWhenMaxValue;
            _Stamina.WarnEconomyUpdated += StaminaUpdate;
            _Direction = Vector2.zero;
            readInputValuesOnInit();
            _AnimatorManager.SetAnimation(_Direction, AimationType.Movement);
            // Active Input System
            _Player1["Move"].performed += Move;
            _Player1["Move"].canceled += Iddle;
            _Player1["Dash"].performed += StartDash;
            _Player1["Dash"].canceled += CancelDash;
            _Player2["Atack"].performed += AtackListener;
            _Player1["Interact"].started += InteractListener;
            _Player2["Inventory"].started += UsableListener;
            base.Init();
        }

        private void UsableListener(InputAction.CallbackContext context)
        {
            int Inputnum = context.action.GetBindingIndexForControl(context.control);
            if (GetComponent<PlayerInventory>().MyObjects.Count() > Inputnum)
                if (GetComponent<PlayerInventory>().MyObjects[Inputnum]._MyTypyItem == ItemScriptableObject.ItemType.Usable)
                {
                    GetComponent<PlayerInventory>().UseObject(GetComponent<PlayerInventory>().MyObjects[Inputnum]);
                }
        }

        private void StartDash(InputAction.CallbackContext context)
        {
            if (_Stamina.Value > _Stamina.MinValue)
            {
                PutDrainStaminaOnlyWhenMove();
                _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
                _CurrentVelocty = _Velocity * _Dash;
                _Rigidbody2D.velocity += _Direction * _CurrentVelocty;
            }
        }
        private void CancelDash(InputAction.CallbackContext context)
        {
            if (_Stamina.Value < _Stamina.MaxValue)
                _Stamina.addPerSeconds(_GainedStaminaPerSeconds,"Gains", GetComponent<PlayerEconomy>());
            _Stamina.StopAddPerSeconds("Dash", GetComponent<PlayerEconomy>());
            _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
            _CurrentVelocty = _Velocity;
            _Rigidbody2D.velocity += _Direction * _CurrentVelocty;
        }
        // Move the Player
        private void Move(InputAction.CallbackContext context)
        {
            _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
            _Direction = context.ReadValue<Vector2>();
            _Rigidbody2D.velocity += _Direction * _CurrentVelocty;
            _AnimatorManager.SetAnimation(_Direction, AimationType.Movement);
            PutDrainStaminaOnlyWhenMove();
        }
        // Stop the Player
        private void Iddle(InputAction.CallbackContext context)
        {
            _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
            _Direction = Vector2.zero;
            _AnimatorManager.SetAnimation(_Direction, AimationType.Movement);
            PutDrainStaminaOnlyWhenMove();
        }
        private void AtackListener(InputAction.CallbackContext context)
        {
            // To Know the Specific Inpt Pressed, to load animation
            _SpecificAtackInput = context.action.GetBindingIndexForControl(context.control);
            if (_StateMachine.GetState<SM_PlayerAtacks>().MyAtacksSize >= _SpecificAtackInput)
            {
                _StateMachine.ChangeState<SM_PlayerAtacks>();
            }
        }
        private void InteractListener(InputAction.CallbackContext context)
        {
            if (GetComponent<SM_PlayerInteraction>().ObjectToInteract.Count > 0)
                _StateMachine.ChangeState<SM_PlayerInteraction>();
        }

        // Read the current Values of Movement and Dash on Init cus we need it if we passed through another State previusly
        private void readInputValuesOnInit()
        {
            // Set CurrentVelocity with Dash if is pressed or simple if not
            if (_Player1["Dash"].IsPressed())
            {
                _Stamina.StopAddPerSeconds("Gains", GetComponent<PlayerEconomy>());
                _Stamina.addPerSeconds(-_DrainedStaminaPerSeconds,"Dash", GetComponent<PlayerEconomy>());
                _CurrentVelocty = _Velocity * _Dash;
            }
            else
            {
                if (_Stamina.Value < _Stamina.MaxValue)
                    _Stamina.addPerSeconds(_GainedStaminaPerSeconds,"Gains", GetComponent<PlayerEconomy>());
                _Stamina.StopAddPerSeconds("Dash", GetComponent<PlayerEconomy>());
                _CurrentVelocty = _Velocity;
            }
            // Set Velocity
            _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
            _Direction = _Player1["Move"].ReadValue<Vector2>();
            _Rigidbody2D.velocity += _Direction * _CurrentVelocty;
        }
        private void PutDrainStaminaOnlyWhenMove()
        {
            if (_Direction != Vector2.zero && _Player1["Dash"].IsPressed())
            {
                _Stamina.StopAddPerSeconds("Gains", GetComponent<PlayerEconomy>());
                _Stamina.addPerSeconds(-_DrainedStaminaPerSeconds,"Dash", GetComponent<PlayerEconomy>());
            }
            else
            {
                _Stamina.addPerSeconds(_GainedStaminaPerSeconds,"Gains", GetComponent<PlayerEconomy>());
                _Stamina.StopAddPerSeconds("Dash", GetComponent<PlayerEconomy>());
            }
        }
        private void StaminaWhenMinValue()
        {
            if (_Player1["Dash"].IsPressed())
            {
                _Stamina.StopAddPerSeconds("Dash", GetComponent<PlayerEconomy>());
                _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
                _CurrentVelocty = _Velocity;
                _Rigidbody2D.velocity += _Direction * _CurrentVelocty;
            }
        }
        private void StaminaWhenMaxValue()
        {
            _Stamina.StopAddPerSeconds("Gains", GetComponent<PlayerEconomy>());
        }

        private void StaminaUpdate()
        {
            _UIManager.UpdateUIEconomyValues(UiItem.StaminaBar, _Stamina.Value);
        }

        public override void Exit()
        {
            // Stamina
            _Stamina.StopAddPerSeconds("Gains", GetComponent<PlayerEconomy>());
            //_Stamina.addPerSeconds(_GainedStaminaPerSeconds,"Gains", GetComponent<PlayerEconomy>()); //comentado porque petaba al morir el player contra un enemigo H
            _Stamina.StopAddPerSeconds("Dash", GetComponent<PlayerEconomy>());
            _Stamina.WarnWhenMinValue -= StaminaWhenMinValue;
            _Stamina.WarnWhenMaxValue -= StaminaWhenMaxValue;
            // Stop Velocity
            _Rigidbody2D.velocity -= _Direction * _CurrentVelocty;
            // Desactive Input System
            _Player1["Move"].performed -= Move; // comentado pq sino no se movia el player al cargar
            _Player1["Move"].canceled -= Iddle; // comentado pq sino no parava cuando se movia el player al cargar
            _Player1["Dash"].performed -= StartDash;
            _Player1["Dash"].canceled -= CancelDash;
            _Player2["Atack"].performed -= AtackListener;
            _Player1["Interact"].started -= InteractListener;
            _Player2["Inventory"].started -= UsableListener;
            _Rigidbody2D.velocity = Vector2.zero;
            base.Exit();
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.layer.Equals(LayerMask.NameToLayer("Interactable")))
                GetComponent<SM_PlayerInteraction>().ObjectToInteract.Add(col.gameObject);
        }
        void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("Interactable")))
                GetComponent<SM_PlayerInteraction>().ObjectToInteract.Remove(other.gameObject);
        }
        void OnCollisionExit2D(Collision2D other)
        {
            _Rigidbody2D.velocity = _Direction * _CurrentVelocty;
        }
    }
}
