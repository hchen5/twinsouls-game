
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using savegame;
using SpecialAtacks;
using UnityEngine;
using static SpecialAtacks.Atacks;

namespace PlayerNameSpace
{
    public class PlayerAtacks : MonoBehaviour
    {
        [Header("Atack Settings")]
        [SerializeField]
        private List<AllAtacksName> _MyAtacksName;
        [SerializeField]
        private int _AtacksCapacity;
        public int AtacksCapacity => _AtacksCapacity;
        private AllAtacksName _AtackToChange;
        private List<Atacks> _MyAtacks = new List<Atacks>();
        public ReadOnlyCollection<Atacks> MyAtacks { get {return _MyAtacks.AsReadOnly();} }
        public int SpecificAtackInput { get {return (int)GetComponent<SM_PlayerMovement>().SpecificAtackInput;} }
        [SerializeField]
        private Pool _MyAtacksItemsPool;
        [SerializeField]
        private GameObject _MyAtacksPoolManager;
        public GameObject  MyAtacksPoolManager => _MyAtacksPoolManager;

        void Start()
        {
            ListAtacksNameToAtacks();
            setUIAtackSprite();
        }

        private void ListAtacksNameToAtacks()
        {
            foreach(AllAtacksName atk in _MyAtacksName)
            {
                Atacks Myatk = NewAtack(gameObject, atk);
                _MyAtacks.Add(Myatk);
                Myatk.WarnWhenStaminaDrained += DrainedStamina;
                Myatk.WarnWhenManaDrained += DrainedMana;
                Myatk.WarnWhenHealthDrained += DrainedHealth;
            }
        }

        private void setUIAtackSprite()
        {
            for(int i = 0; i < _MyAtacksName.Count; i++)
            {
                GetComponent<PlayerUI>().UpdateAtackSprite(i, _MyAtacks[i].GetMyAtackSprite());
            }
        }

        private void DrainedStamina(float num)
        {
            GetComponent<PlayerEconomy>()._Stamina.add(-num);
        }
        private void DrainedMana(float num)
        {
            GetComponent<PlayerEconomy>()._Mana.add(-num);
            GetComponent<PlayerUI>().UpdateUIEconomyValues(PlayerUI.UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.Value);
        }
        private void DrainedHealth(float num)
        {
            GetComponent<PlayerEconomy>()._Health.add(-num);
        }

        public void setAtackToChange(AllAtacksName NameAtack)
        {
            _AtackToChange = NameAtack;
        }

        public void ChangeAtack(int Position)
        {
            Atacks Myatk = NewAtack(gameObject, _AtackToChange);
            _MyAtacks[Position].WarnWhenStaminaDrained -= DrainedStamina;
            _MyAtacks[Position].WarnWhenManaDrained -= DrainedMana;
            _MyAtacks[Position].WarnWhenHealthDrained -= DrainedHealth;
            _MyAtacks.RemoveAt(Position);
            _MyAtacks.Insert(Position, Myatk);
            Myatk.WarnWhenStaminaDrained += DrainedStamina;
            Myatk.WarnWhenManaDrained += DrainedMana;
            Myatk.WarnWhenHealthDrained += DrainedHealth;
        }
        public void AddAtack()
        {
            Atacks Myatk = NewAtack(gameObject, _AtackToChange);
            Myatk.WarnWhenStaminaDrained += DrainedStamina;
            Myatk.WarnWhenManaDrained += DrainedMana;
            Myatk.WarnWhenHealthDrained += DrainedHealth;
            _MyAtacks.Add(Myatk);
        }
        public void SpawnItemAtack(AtacksScriptableObjects atk, Vector3 pos)
        {
            GameObject obj = _MyAtacksItemsPool.GetElement();
            obj.GetComponentInChildren<ItemAtack>().SetMyAtack(atk);
            obj.transform.position = pos;
        }
        public void ReturnElementToApool(GameObject atk)
        {
            _MyAtacksItemsPool.ReturnElement(atk);
        }
    }
}

