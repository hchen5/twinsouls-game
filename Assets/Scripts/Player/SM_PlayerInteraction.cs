using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Animations;
using EconomyManager;
using FSMState;
using Items;
using SpecialAtacks;
using Unity.VisualScripting;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.InputSystem;
using World;
using static Animations.AnimatorManager;
using static PlayerNameSpace.PlayerUI;
using static SpecialAtacks.Atacks;

namespace PlayerNameSpace
{
    public class SM_PlayerInteraction : MBState
    {
        // GameObject Components
        // new
        private NewFiniteStateMachine _StateMachine;
        internal List<GameObject> ObjectToInteract;
        private GameObject _myObject;
        private InputActionMap _Player1;
        private InputActionMap _Player2;

        private void Awake()
        {
            _Player1 = GetComponent<SM_PlayerInteraction>()._Player1;
            _Player2 = GetComponent<SM_PlayerInteraction>()._Player2;
            _StateMachine = GetComponent<NewFiniteStateMachine>();
            ObjectToInteract = new List<GameObject>();
        }

        public override void Init()
        {
            DoWhatInteraction();
            base.Init();
        }

        private void AtackListener(InputAction.CallbackContext context)
        {
            int PositionInList = (int)context.action.GetBindingIndexForControl(context.control) - 1;
            if (!GetComponent<PlayerAtacks>().MyAtacks[PositionInList].CoolDown() && GetComponent<PlayerAtacks>().AtacksCapacity >= PositionInList)
            {
                GetComponent<PlayerAtacks>().SpawnItemAtack(GetComponent<PlayerAtacks>().MyAtacks[PositionInList].MyAtack, _myObject.transform.position);
                GetComponent<PlayerAtacks>().setAtackToChange(_myObject.GetComponent<ItemAtack>().MyAtack.Atack);
                GetComponent<PlayerAtacks>().ChangeAtack(PositionInList);
                GetComponent<PlayerAtacks>().ReturnElementToApool(_myObject.transform.parent.gameObject);
                GetComponent<PlayerUI>().UpdateAtackSprite(PositionInList, _myObject.GetComponent<ItemAtack>().MyAtack.AtackImage);
            }
            _StateMachine.ChangeState<SM_PlayerMovement>();
        }

        private void InventaryListener(InputAction.CallbackContext context)
        {
            int PositionInList = (int)context.action.GetBindingIndexForControl(context.control);
            if (GetComponent<PlayerInventory>().Capacity >= PositionInList)
            {
                GetComponent<PlayerInventory>().SpawnItemAtack(GetComponent<PlayerInventory>().MyObjects[PositionInList], _myObject.transform.position);
                GetComponent<PlayerInventory>().ChangeObject(PositionInList, _myObject.GetComponent<ItemObject>().myItem);
                GetComponent<PlayerInventory>().ReturnToApool(_myObject.transform.parent.gameObject);
                GetComponent<PlayerUI>().UpdateItemSprite(PositionInList, _myObject.GetComponent<ItemObject>().myItem.ItemImage);
            }
            _StateMachine.ChangeState<SM_PlayerMovement>();
        }

        private void ChangeToMovement(InputAction.CallbackContext context)
        {
            _StateMachine.ChangeState<SM_PlayerMovement>();
        }
        private void DoWhatInteraction()
        {
            _myObject = NearInteractableObject();
            switch (_myObject.tag)
            {
                case "Atack":
                    GetAtack();
                    break;
                case "Object":
                    GetObject();
                    break;
                case "WorldInteractable":
                    WorldInteract();
                    break;
                default:
                    break;
            }
        }

        private void WorldInteract()
        {
            WorldInteractable w = _myObject.GetComponent<WorldInteractable>();
            w.Interact(gameObject);
            _StateMachine.ChangeState<SM_PlayerMovement>();
        }

        private void GetObject()
        {
            if (GetComponent<PlayerInventory>().Capacity > GetComponent<PlayerInventory>().MyObjects.Count())
            {
                GetComponent<PlayerInventory>().addObject(_myObject.GetComponent<ItemObject>().myItem);
                GetComponent<PlayerInventory>().ReturnToApool(_myObject.transform.parent.gameObject);
                GetComponent<PlayerUI>().UpdateItemSprite(GetComponent<PlayerInventory>().MyObjects.Count() - 1, _myObject.GetComponent<ItemObject>().myItem.ItemImage);
                _StateMachine.ChangeState<SM_PlayerMovement>();
            }
            else
            {
                _Player2["Inventory"].canceled += InventaryListener;
                _Player1["Move"].performed += ChangeToMovement;
                _Player1["Dash"].performed += ChangeToMovement;
            }
        }

        private void GetAtack()
        {
            if (GetComponent<PlayerAtacks>().AtacksCapacity > GetComponent<PlayerAtacks>().MyAtacks.Count())
            {
                GetComponent<PlayerAtacks>().setAtackToChange(_myObject.GetComponent<ItemAtack>().MyAtack.Atack);
                GetComponent<PlayerAtacks>().AddAtack();
                GetComponent<PlayerAtacks>().ReturnElementToApool(_myObject.transform.parent.gameObject);
                GetComponent<PlayerUI>().UpdateAtackSprite(GetComponent<PlayerAtacks>().MyAtacks.Count() - 1, _myObject.GetComponent<ItemAtack>().MyAtack.AtackImage);
                _StateMachine.ChangeState<SM_PlayerMovement>();
            }
            else
            {
                _Player2["Atack"].canceled += AtackListener;
                _Player1["Move"].performed += ChangeToMovement;
                _Player1["Dash"].performed += ChangeToMovement;
            }
        }
        private GameObject NearInteractableObject()
        {
            GameObject myObject = null;
            float MinDis = float.MaxValue;
            foreach (GameObject obj in ObjectToInteract)
            {
               float Distance = Mathf.Abs((transform.position - obj.transform.position).magnitude);
               if (Distance < MinDis)
               {
                    myObject = obj;
                    MinDis = Distance;
               }
            }
            return myObject;
        }
        public override void Exit()
        {
            // Desactive Input System
            if (_Player2 != null && _Player1 != null)
            {
                _Player2["Atack"].canceled -= AtackListener;
                _Player2["Inventory"].canceled -= InventaryListener;
                _Player1["Move"].performed -= ChangeToMovement;
                _Player1["Dash"].performed -= ChangeToMovement;
            }
            _myObject = null;
            base.Exit();
        }
    }
}
