using System;
using FSMState;
using savegame;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using static PlayerNameSpace.PlayerUI;
using static savegame.SaveData;

namespace PlayerNameSpace
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CircleCollider2D))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(PlayerAtacks))]
    [RequireComponent(typeof(PlayerEconomy))]
    [RequireComponent(typeof(PlayerInventory))]
    [RequireComponent(typeof(PlayerUI))]
    [RequireComponent(typeof(SM_PlayerAtacks))]
    [RequireComponent(typeof(SM_PlayerMovement))]
    [RequireComponent(typeof(SM_PlayerInteraction))]
    [RequireComponent(typeof(NewFiniteStateMachine))]
    public class Player : MonoBehaviour ,ISaveablePlayer
    {
        [Header("Movement Settings")]
        [SerializeField]
        private int _Velocity = 3;
        public int Velocity { get { return _Velocity; }}
        [SerializeField]
        private float _Dash = 2;
        public float Dash { get { return _Dash; }}

        private NewFiniteStateMachine _StateMachine;
        private void Awake()
        {
            _StateMachine = GetComponent<NewFiniteStateMachine>();
        }

        private void Start()
        {
            GetComponent<PlayerEconomy>()._Health.WarnWhenMinValue += Dead;
            GetComponent<PlayerEconomy>()._Health.WarnEconomyUpdated += Update;
            SetStateMovement();
        }

        public void SetStateMovement()
        {
            _StateMachine.ChangeState<SM_PlayerMovement>();
        }
        private void Dead()
        {
            Debug.Log("Dead");
            GameManager.Instance.MenuScene();
        }
        private void Update()
        {
            GetComponent<PlayerUI>().UpdateUIEconomyValues(UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.Value);
        }

        public SaveData.PlayerData Save()
        {
            return new PlayerData(transform.position, (int)GetComponent<PlayerEconomy>()._Health.Value);
        }

        public void Load(SaveData.PlayerData _playerdata)
        {
            transform.position = _playerdata.position;
            GetComponent<PlayerEconomy>()._Health.add(- GetComponent<PlayerEconomy>()._Health.Value + _playerdata.vida);
            GetComponent<PlayerUI>().UpdateUIEconomyValues(UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.Value);
        }
        private void OnDestroy()
        {
            _StateMachine.GetState<SM_PlayerMovement>().Exit();
            _StateMachine.GetState<SM_PlayerInteraction>().Exit();
            _StateMachine.GetState<SM_PlayerAtacks>().Exit();

        }
    }
}

