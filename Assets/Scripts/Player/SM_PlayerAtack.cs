using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Animations;
using EconomyManager;
using FSMState;
using SpecialAtacks;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEditor.VersionControl;
using UnityEngine;
using static Animations.AnimatorManager;
using static SpecialAtacks.Atacks;

namespace PlayerNameSpace
{
    public class SM_PlayerAtacks : MBState
    {
        // GameObject Components
        private Animator _Animator;
        // Manager Variables
        private AnimatorManager _AnimatorManager;
        // 1 = BasicAtack | 2 = Slot Atack | 3 = Slot Atack ... (¡Animations!), The slots can be MeleAtack and SpellAtack, depends of the equiped habilitys of the Player
        private ReadOnlyCollection<Atacks> _MyAtacks;
        public int MyAtacksSize { get {return _MyAtacks.Count; } }
        // new
        private NewFiniteStateMachine _StateMachine;

        private void Awake()
        {
            _MyAtacks = GetComponent<PlayerAtacks>().MyAtacks;
            _Animator = GetComponent<Animator>();
            _AnimatorManager = new AnimatorManager(_Animator);
            _StateMachine = GetComponent<NewFiniteStateMachine>();
        }
        public override void Init()
        {
            _MyAtacks = GetComponent<PlayerAtacks>().MyAtacks;
            if (!CheckCoolDown())
                ChooseAnimationAtack();
            else
                _StateMachine.ChangeState<SM_PlayerMovement>();
            base.Init();
        }
        // Check if Atack can be used
        private bool CheckCoolDown()
        {
            int SpecificAtackInput = (int)_StateMachine.GetState<SM_PlayerMovement>().SpecificAtackInput;
            if (SpecificAtackInput == 0)
                return false;
            else
                return _MyAtacks[(int)SpecificAtackInput - 1].SetCoolDown();
        }
        // Set the Specific Animation Atack
        private void ChooseAnimationAtack()
        {
            // Depending of Type Animation of the Player Atack List we set Mele Animation, Spell Animation or Basic Animation
            Vector2 Direction = _StateMachine.GetState<SM_PlayerMovement>().LookingToDirection;
            int SpecificAtackInput = (int)_StateMachine.GetState<SM_PlayerMovement>().SpecificAtackInput;
            if (SpecificAtackInput == 0)
                _AnimatorManager.SetAnimation(Direction, AimationType.BasicAtack);
            else
            {
                if (_MyAtacks[(int)SpecificAtackInput - 1].MyAtackType == AtackType.Mele)
                    _AnimatorManager.SetAnimation(Direction, AimationType.MeleAtack);
                else
                    _AnimatorManager.SetAnimation(Direction, AimationType.SpellAtack);
            }
        }
        // Calling the Specific Special Atack
        public void Atack()
        {
            if (_MyAtacks[(int)_StateMachine.GetState<SM_PlayerMovement>().SpecificAtackInput - 1].MyAtack.DrainedMana! < GetComponent<PlayerEconomy>()._Mana.Value)
            {
                _MyAtacks[(int)_StateMachine.GetState<SM_PlayerMovement>().SpecificAtackInput - 1].Atack();
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
