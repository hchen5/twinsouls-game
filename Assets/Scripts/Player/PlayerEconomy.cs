using EconomyManager;
using UnityEngine;

namespace PlayerNameSpace
{
    public class PlayerEconomy : MonoBehaviour
    {
        internal Economy _Health;
        internal Economy _Stamina;
        internal Economy _Mana;

        [Header("Health Settings")]
        [SerializeField]
        private float _HealthValue;
        [SerializeField]
        private float _MaxHealth;
        [SerializeField]
        private float _MinHealth;
        [Header("Stamina Settings")]
        [SerializeField]
        private float _StaminaValue;
        [SerializeField]
        private float _MaxStamina;
        [SerializeField]
        private float _MinStamina;
        [SerializeField]
        private float _DrainedStaminaPerSeconds;
        public float DrainedStaminaPerSeconds { get {return _DrainedStaminaPerSeconds;} }
        [SerializeField]
        private float _GainedStaminaPerSeconds;
        public float GainedStaminaPerSeconds { get {return _GainedStaminaPerSeconds;} }
        [Header("Mana Settings")]
        [SerializeField]
        private float _ManaValue;
        [SerializeField]
        private float _MaxMana;
        [SerializeField]
        private float _MinMana;
        [SerializeField]
        private float _GainedManaPerSeconds;
        public float GainedManaPerSeconds { get {return _GainedManaPerSeconds;} }

        void Awake()
        {
            GenerateEconomy();
        }

        private void GenerateEconomy()
        {
            _Health = new Economy(_HealthValue, _MinHealth, _MaxHealth);
            _Stamina = new Economy(_StaminaValue, _MinStamina, _MaxStamina);
            _Mana = new Economy(_ManaValue, _MinMana, _MaxMana);
        }
        public void ResetEconomy()
        {
            _Health = new Economy(_HealthValue, _MinHealth, _MaxHealth);
            _Stamina = new Economy(_StaminaValue, _MinStamina, _MaxStamina);
            _Mana = new Economy(_ManaValue, _MinMana, _MaxMana);
        }
    }
}
