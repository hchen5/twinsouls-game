using System.Collections;
using System.Collections.Generic;
using PlayerNameSpace;
using SpecialAtacks;
using Unity.VisualScripting;
using UnityEngine;
using static PlayerNameSpace.PlayerUI;

namespace  PlayerNameSpace
{
    public class PlayerCollisions : MonoBehaviour
    {
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.layer.Equals(LayerMask.NameToLayer("EnemyHitBox")))
                GetComponent<PlayerEconomy>()._Health.add(-other.gameObject.GetComponent<AtackObjectScript>().Damage);
        }
    }
}

