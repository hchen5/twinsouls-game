using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Items;
using savegame;
using Unity.VisualScripting;
using UnityEngine;

namespace PlayerNameSpace
{
    public class PlayerInventory : MonoBehaviour, ISaveableInventory
    {
        [SerializeField]
        private ItemsDb _ItemsDB;
        [SerializeField]
        private List<ItemScriptableObject> _MyObjects;
        public ReadOnlyCollection<ItemScriptableObject> MyObjects { get {return _MyObjects.AsReadOnly();} }
        [SerializeField]
        private int _Capacity;
        public int Capacity => _Capacity;
        [SerializeField]
        private Pool _MyObjectsPool;
        private bool _Check = false;
        private List<ItemScriptableObject> _MyObjectsLoads;
        void Start()
        {
            if (!_Check)
            {
                Debug.Log("check");
                AddObjectsInList();
            }
            else
                _MyObjects = _MyObjectsLoads;
                
        }
        public void AddObjectStats(ItemScriptableObject item)
        {
            GetComponent<PlayerEconomy>()._Health.IncreaseMaxvalue(item._GainHealth);
            GetComponent<PlayerEconomy>()._Mana.IncreaseMaxvalue(item._GainMana);
            GetComponent<PlayerEconomy>()._Stamina.IncreaseMaxvalue(item._GainStamina);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.MaxValue);
        }
        private void AddObjectsInList()
        {
            int count = 0;
            foreach (ItemScriptableObject item in _MyObjects)
            {
                GetComponent<PlayerUI>().UpdateItemSprite(count, item.ItemImage);
                if (item._MyTypyItem == ItemScriptableObject.ItemType.Object)
                    AddObjectStats(item);
                count++;
            }
        }
        public void RemoveObjectStats(ItemScriptableObject item)
        {
            GetComponent<PlayerEconomy>()._Health.IncreaseMaxvalue(- item._GainHealth);
            GetComponent<PlayerEconomy>()._Mana.IncreaseMaxvalue(- item._GainMana);
            GetComponent<PlayerEconomy>()._Stamina.IncreaseMaxvalue(- item._GainStamina);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.MaxValue);
        }
        public void UseObject(ItemScriptableObject item)
        {
            GetComponent<PlayerEconomy>()._Health.add(item._GainHealth);
            GetComponent<PlayerEconomy>()._Mana.add(item._GainMana);
            GetComponent<PlayerEconomy>()._Stamina.add(item._GainStamina);
            GetComponent<PlayerUI>().UpdateItemSprite(_MyObjects.IndexOf(item), null);
            //AddObjectsInList();//H
            _MyObjects.Remove(item);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.StaminaBar, GetComponent<PlayerEconomy>()._Stamina.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.ManaBar, GetComponent<PlayerEconomy>()._Mana.MaxValue);
            GetComponent<PlayerUI>().UpdateUIEconomyMaxValues(PlayerUI.UiItem.HealthBar, GetComponent<PlayerEconomy>()._Health.MaxValue);

            //AddObjectsInList();

            int c = _MyObjects.Count;
            for (int i = c; i < 6; i++)
            {
                GetComponent<PlayerUI>().UpdateItemSprite(i, null);
            }
            for (int i = 0; i < _MyObjects.Count; i++)
            {
                GetComponent<PlayerUI>().UpdateItemSprite(i, _ItemsDB.GetItem(_MyObjects[i].id).ItemImage);
            }
        }
        public void addObject(ItemScriptableObject obj)
        {
            _MyObjects.Add(obj);
            if (obj._MyTypyItem == ItemScriptableObject.ItemType.Object)
                AddObjectStats(obj);
        }
        public void ChangeObject(int Position, ItemScriptableObject item)
        {
            RemoveObjectStats(_MyObjects[Position]);
            _MyObjects.RemoveAt(Position);
            _MyObjects.Insert(Position, item);
            AddObjectStats(item);
        }
        public void ReturnToApool(GameObject item)
        {
            _MyObjectsPool.ReturnElement(item);
        }
        public void SpawnItemAtack(ItemScriptableObject item, Vector3 pos)
        {
            GameObject obj = _MyObjectsPool.GetElement();
            obj.GetComponentInChildren<ItemObject>().SetMyItem(item);
            obj.transform.position = pos;
        }

        public SaveData.InventoryData Save()
        {
            List<int> itemsid = new List<int>();
            for(int i=0;i<_MyObjects.Count;i++)
            {
                itemsid.Add(_MyObjects[i].id);
            }
            return new SaveData.InventoryData(itemsid);
            //return new SaveData.InventoryData(_MyObjects);
        }

        public void Load(SaveData.InventoryData _InventoryData)
        {
            GetComponent<PlayerEconomy>().ResetEconomy();
            List<int> itemsid =_InventoryData.itemsid;
            _MyObjectsLoads = new();
            _MyObjects.Clear();
            for (int i = 0; i < itemsid.Count; i++) 
            {
                //_MyObjects.Add(_ItemsDB.GetItem(itemsid[i]));
                
                if (_ItemsDB.GetItem(itemsid[i])._MyTypyItem == ItemScriptableObject.ItemType.Object) 
                {
                    addObject(_ItemsDB.GetItem(itemsid[i]));
                }
                GetComponent<PlayerUI>().UpdateItemSprite(i, _ItemsDB.GetItem(itemsid[i]).ItemImage);
                // addObject(_ItemsDB.GetItem(itemsid[i]));
                _MyObjectsLoads.Add(_ItemsDB.GetItem(itemsid[i]));
            }
            _Check = true;
            /*
            foreach(ItemScriptableObject i in _MyObjects)
            {
                Debug.Log(i.name);
            }*/
        }
    }
}

